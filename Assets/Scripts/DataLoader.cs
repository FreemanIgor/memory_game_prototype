﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;

public class DataLoader : MonoBehaviour
{
    #region Singleton
    public static DataLoader instance;
    private void Awake()
    {
        instance = this;
    }
    #endregion
    private string cardsConfigURL = "https://script.googleusercontent.com/macros/echo?user_content_key=AosoJHHTycw4Yl2fXHIcfpYL5IERtDMRhomEqGDQ5b5Th3vh2U0GTdLLyTEv8uPKx8xD5PJB4PImeg9gsu2M2GQPHISt-aJVm5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnEoIO6TzJBWyeVKozFhDel7HnjncNUmjKJKHzGxKwmyXnWKVmg1wzIUi0egDWkNg1ey9a8ZK7y-C&lib=MAtrOhtPwC9L4noPamX2FHEiOGXGv80R6";
    public Cards cards;
    void Start()
    {
        StartCoroutine(ReadCardsConfig());
    }
    IEnumerator ReadCardsConfig()
    {
        UnityWebRequest www = UnityWebRequest.Get(cardsConfigURL);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
            Debug.Log(www.error);
        else cards = JsonConvert.DeserializeObject<Cards>(www.downloadHandler.text);
    }
}
[System.Serializable]
public class Card
{
    public string name;
    public string URL;
}
[System.Serializable]
public class Cards
{
   public Card[] cards;
}
