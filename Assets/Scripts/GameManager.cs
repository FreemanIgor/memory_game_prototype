﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region Singleton
    public static GameManager instance;
    private void Awake()
    {
        instance = this;
    }
    #endregion

    public CardController[] cards;
    public CardController firstTurn { get; set; }
    public CardController secondTurn { get; set; }
    private int turn = 0;
    public int points { get; private set; } = 0;
    public bool isCanTapCard { get; set; } = true;

    public delegate  void OnGameStarted();
    public event OnGameStarted onGameStarted;

    public delegate void OnGameFinished();
    public event OnGameFinished onGameFinished;

    public delegate void OnPointsCountChanged();
    public event OnPointsCountChanged onPointsCountChanged;

    public delegate void OnRandomizeCards(string imageURL, Image image);
    public event OnRandomizeCards onRandomizeCards;


    void Start()
    {
        StartCoroutine(StartGame());
    }
    IEnumerator  StartGame()
    {
        points = 0;
        yield return new WaitForSeconds(3.5f);
        RandomizeCards();
        onGameStarted?.Invoke();
    }
    public void RestartGame()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
    void RandomizeCards()
    {
        List<Card> _cards = new List<Card>();
        for (int i = 0; i < DataLoader.instance.cards.cards.Length; i++)
            if (DataLoader.instance.cards.cards[i].name != "CardBack")
            {
                _cards.Add(DataLoader.instance.cards.cards[i]);
                _cards.Add(DataLoader.instance.cards.cards[i]);
            }
        for (int i = 0; i < cards.Length; i++)
        {
            int random = Random.Range(0, _cards.Count);
            cards[i].cardName = _cards[random].name;
            cards[i].name = _cards[random].name;
            onRandomizeCards?.Invoke(_cards[random].URL, cards[i].image);
            _cards.Remove(_cards[random]);
        }
    }
    IEnumerator _MakeTurn(CardController cardController)
    {
        turn++;

        if (turn == 1)
            firstTurn = cardController;
        else if (turn == 2)
        {
            turn = 0;
            secondTurn = cardController;
            if (firstTurn.cardName == secondTurn.cardName)
            {
                yield return new WaitForSeconds(1f);
                firstTurn.gameObject.SetActive(false);
                secondTurn.gameObject.SetActive(false);
                points++;
                onPointsCountChanged?.Invoke();
                if (points == 3)
                    onGameFinished?.Invoke();
            }
            else
            {
                yield return new WaitForSeconds(1f);
                firstTurn.RotateCard(false);
                secondTurn.RotateCard(false);
            }
           
        }
        yield return new WaitForSeconds(1f);
        isCanTapCard = true;
    }
    public void MakeTurn(CardController cardController)
    {
        StartCoroutine(_MakeTurn(cardController));
    }
}

