﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class GameView : MonoBehaviour
{
    GameManager gameManager;
    [SerializeField] Text scoreText;
    [SerializeField] Text countdownText;
    [SerializeField] GameObject restartGameButton;
    [SerializeField] Animator appearenceAnim;
    int countdown = 3;
    void Start()
    {
        gameManager = GameManager.instance;
        gameManager.onGameStarted += OnGameStarted;
        gameManager.onGameFinished += OnGameFinished;
        gameManager.onPointsCountChanged += OnPointsCountChanged;
        gameManager.onRandomizeCards += OnRandomizeCards;
    }
    void OnGameStarted()
    {
        StartCoroutine(_OnGameStarted());
    }
    IEnumerator _OnGameStarted()
    {
        scoreText.text = "Score: " + 0.ToString();
        for (int i = 0; i < DataLoader.instance.cards.cards.Length; i++)
            if (DataLoader.instance.cards.cards[i].name == "CardBack")
            {
                for (int j = 0; j < gameManager.cards.Length; j++)
                    StartCoroutine(_GetImage(DataLoader.instance.cards.cards[i].URL, gameManager.cards[j].backImage));
                break;
            }
        countdownText.gameObject.SetActive(true);
        for (int i = 0; i < 3; i++)
        {
            yield return new WaitForSeconds(1f);
            countdown -= 1;
            countdownText.text = countdown.ToString();
        }
        countdownText.gameObject.SetActive(false);
        appearenceAnim.enabled = true;
        yield return new WaitForSeconds(5f);
        for (int i = 0; i < gameManager.cards.Length; i++)
            gameManager.cards[i].RotateCard(false);
    }
    void OnGameFinished()
    {
        restartGameButton.SetActive(true);
    }
    void OnPointsCountChanged()
    {
        scoreText.text = "Score: " + gameManager.points.ToString();
    }
    void OnRandomizeCards(string imageURL, Image image)
    {
        GetImage(imageURL, image);
    }
    void GetImage(string imageURL, Image image)
    {
        StartCoroutine(_GetImage(imageURL, image));
    }
    IEnumerator _GetImage(string imageURL, Image image)
    {
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(imageURL))
        {
            yield return uwr.SendWebRequest();
            if (uwr.isNetworkError || uwr.isHttpError)
                Debug.Log(uwr.error);
            else
            {
                var tex = DownloadHandlerTexture.GetContent(uwr);
                Sprite mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                image.sprite = mySprite;
            }
        }
    }
}
