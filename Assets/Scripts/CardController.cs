﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class CardController : MonoBehaviour, IPointerClickHandler
{
    public string cardName { get; set; }
    public Image image { get; set; }
    public Image backImage { get; set; }
    private int yRotation = 0;
    bool isCoroutineWork;
    void Start()
    {
        //backImage = GetComponentInChildren<Image>(); // WTF?
        backImage = transform.GetChild(0).GetComponent<Image>();
        image = GetComponent<Image>();
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if (GameManager.instance.isCanTapCard == true)
            RotateCard(true);
    }
    IEnumerator _RotateCard(bool isLeft)
    {
        isCoroutineWork = true;
        for (int i = 0; i < 45; i++)
        {
            yRotation = 4;
            yield return null;
            transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y + yRotation, 0);
            if (i == 22)
            {
                if (isLeft) 
                {
                    backImage.gameObject.SetActive(false);
                    GameManager.instance.isCanTapCard = false;
                }
                else backImage.gameObject.SetActive(true);
            }
        }
        if (isLeft) GameManager.instance.MakeTurn(this);
        isCoroutineWork = false;
    }
    public void RotateCard(bool isleft)
    {
        if (isCoroutineWork == false)
        StartCoroutine(_RotateCard(isleft));
    }
}

